
package ru.peo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.Vector;

/**
 *Данная программа представляет собой "Телефонный справочник". С помощью нее сделаем поиск по БД более приятным,
 *не зависимым от знаний языка SQL пользователя, благодаря удобному графическому интерфейсу.
 * 
 * Создадим подкласс Windows класса JFrame, наследуя от JFrame мы получим всю функциональность окна.
 * Имплементируем слушателя ActionListener для дальнейшей работы с обработчником событий
 * */
public class Windows extends JFrame implements ActionListener{
    /** создаем объект панели */
        JPanel panel = new JPanel();
    /**создаем поле с текстом*/
        JLabel textoutputfield = new JLabel("<html><font size = 4>Введите ");
     /**создаем поле для ввода*/
        JTextField inputfield = new JTextField(20);
     /**создаем кнопку с иконкой*/
    JButton pressme = new JButton(new ImageIcon("233.jpg"));
     /**создадим выпающий список:*/
        String[] mod = {"ФИО","Телефон","Отдел","Вывести всех"};//массив содержащий в себе наименования разделов выпадающего списка
        JComboBox cb = new JComboBox(mod);//сам выпадающий список
     /**Создадим модель таблицы*/
        DefaultTableModel tableModel = new DefaultTableModel();
     /**На основе модели, создадим новую JTable*/
        JTable table = new JTable(tableModel);

     /**Конструктор класса*/
        Windows()
        {
            /**Вызываем конструктор суперкласса и передаем ему параметр, в данном случае имя программы*/
            super("Телефонный справочник");
             /**метод указывает операцию, которая будет произведена при закрытии окна, в данном случае выход из программы.*/
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
             /**задаем размер окна*/
            setSize(650, 500);
             /**фиксированный размер окна*/
            setResizable(false);
             /**задаем цвет панели*/
            panel.setBackground(Color.WHITE);
             /**Делаем окно по центру*/
            setLocationRelativeTo(null);
             /**наследует главный кадр*/
            Container con = this.getContentPane();
             /** add- Этот метод получает в качестве параметра указанный компонент и добавляет его*/
            con.add(panel);
            panel.add(textoutputfield);
            panel.add(cb);
            panel.add(inputfield);
             /** Убираем все ненужные рамки и закраску у кнопки*/
            pressme.setBorderPainted(false);
            pressme.setFocusPainted(false);
            pressme.setContentAreaFilled(false);
            panel.add(pressme);
            /** регистрируем слушатель кнопки*/
            pressme.addActionListener(this);
            panel.add(table);
            /**Создаем панель прокрутки и включаем ее в свою таблицу, указываем расположение на окне с помощью менеджера размещения*/
            con.add(new JScrollPane(table), BorderLayout.SOUTH);
            /** делаем кадр видимым*/
            setVisible(true);
        }
    /** здесь находится базовый обработчик события
    *метод actionPerformed(), получающий в качестве параметра объект класса ActionEvent.*/
        public void actionPerformed(ActionEvent e) {
            /**Получение источника события*/
            Object source = e.getSource();
            /**Сравниваем полученный источник с кнопкой pressme - для обработки события*/
            if (source == pressme) {
                /** Поле ввода заполнено (длина строки не нулевая) или в выпадающем списке выбрано "Вывести всех" */
                if (inputfield.getText().length() != 0 || cb.getSelectedIndex() == 3) {
                    String sQuery=""; //Объявление и инициализация переменной с запросом
                    /** Обработка выбора пользователя */
                    switch(cb.getSelectedIndex()) {
                        case 0://Формирование запроса с фильтром по ФИО
                            /**
                             * SELECT a.id as ID,b.name as Отдел,a.name as Имя,a.phone as Телефон   //Выбор полей
                             *     FROM workers a                                                   //Из таблицы Workers(работники) с псевдоним(алиас) "a"
                             *     JOIN departments b ON a.id_dept = b.id                           //Объединение с таблицей Departments(отделы) c алисом "b"
                             *                                                                      //объединение проходит по полям id_dept таблицы Workers
                             *                                                                      //и id из Departments
                             *     where a.name like 'А%'                                           //Условие фильтрации где ФИО подобно переменной
                             *                                                                      //% - любое количество любых символов
                             *     ORDER BY a.id                                                    //Устанавливается порядок сортировки по первому полю в
                             *                                                                      //выборке - в нашем случае по a.id
                             */
                            sQuery = "SELECT a.id as ID,b.name as Отдел,a.name as Имя,a.phone as Телефон FROM workers a JOIN departments b ON a.id_dept = b.id where a.name like '%"+inputfield.getText()+"%' ORDER BY a.id";
                            break; // Прервать обработку оператора switch
                        case 1://Формирование запроса с фильтром по Телефону
                            sQuery = "SELECT a.id as ID,b.name as Отдел,a.name as Имя,a.phone as Телефон FROM workers a JOIN departments b ON a.id_dept = b.id where a.phone like '%"+inputfield.getText()+"%' ORDER BY a.id";
                            break; // Прервать обработку оператора switch
                        case 2://Формирование запроса с фильтром по Отделу
                            sQuery = "SELECT a.id as ID,b.name as Отдел,a.name as Имя,a.phone as Телефон FROM workers a JOIN departments b ON a.id_dept = b.id where b.name like '%"+inputfield.getText()+"%' ORDER BY a.id";
                            break; // Прервать обработку оператора switch
                        default://Формирование запроса без фильтра
                            sQuery = "SELECT a.id as ID,b.name as Отдел,a.name as Имя,a.phone as Телефон FROM workers a JOIN departments b ON a.id_dept = b.id ORDER BY a.id";
                            break; // Прервать обработку оператора switch
                    }
                    /**Заполнение таблицы*/
                    FillTable(sQuery);
                } //if - выбор действия по выподающему списку
            } //if - выбор источника
        }

    /**
     * Процедура заполнения таблицы по запросу
     * @param Query - запрос SQL
     */
    public void FillTable(String Query){
        /**Попытка заполнения таблицы*/
        try
        {
            /** Создание соединения */
            Connection conn;
            /** регистрация драйвера */
            Class.forName("org.sqlite.JDBC");
            /** метод DriverManager.getConnection, которому передаются параметры соединения с БД сразу после регистрации драйвера JDBC устанавливает соединение с БД */
            conn = DriverManager.getConnection("jdbc:sqlite:test.sqlite");//указание файла базы - test.sqlite
            /**Создание "интерфейса" в соединении conn (для выполнения запросов, позволяет выполнять запросы в БД)*/
            Statement stat = conn.createStatement();
            /**Результат запроса, хранящегося в переменной Query, мы заносим в rs */
            ResultSet rs = stat.executeQuery(Query);
             /**Вектор с заголовками столбцов тип String */
            Vector<String> columnNames = new Vector<String>();
             /**Получение количества столбцов */
            int columnCount = rs.getMetaData().getColumnCount();
             /**Заполнение вектора columnNames наименованием полей */
            for (int i = 1; i <= columnCount; i++) {
                columnNames.add(rs.getMetaData().getColumnName(i));//
            }
            /**Объявление и заполнение вектора с данными*/
            Vector<Vector<Object>> data = new Vector<Vector<Object>>();
             /**Пока в rs есть строки - выбираем данные*/
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int i = 1; i <= columnCount; i++) {
             /**Добавление данных колонки из rs*/
                    vector.add(rs.getObject(i));
                }
             /**vector - хранит текущую строку*/
             /**Добавляем текущую строку в data - наш вектор с данными*/
                data.add(vector);
            }
             /**Заполняем таблицу данными из data и заголовками из columnNames*/
            tableModel.setDataVector(data, columnNames);
             /**Закрываем rs*/
            rs.close();
             /**Закрываем "интерфейс"*/
            stat.close();
             /**Закрываем соединение*/
            conn.close();
        }
             /**Обработка исключений*/
        catch( SQLException | ClassNotFoundException e)
        {
             /**Печать стека вызова*/
            e.printStackTrace();
        }
    }
	/**Тут программа начинает свою работу */
        public static void main(String args[]) throws ClassNotFoundException, SQLException {
             /**Создание объекта ru.peo.Windows */
            new ru.peo.Windows();}
    }